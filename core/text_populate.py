# -*- coding: utf-8 -*-
#
#  text_poplate.py
#
#  Copyright 2016 Басманов Илья <ffsdmad@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import re
from flask import url_for


def text_populate(data):
    """ Пост обработка текст, обработка символов @ ~ #  """
    from app import models
    from modules.page_editor.models import Page
    for num in re.findall (r'@(\d+)', data):
        azs = models.Azs.query.filter(models.Azs.num == num).first()
        if azs:
            data = data.replace('@'+num, '<a href="{0}" title="{1}">АЗС№ {2}</a>'.format(url_for('azs_map.index', azs=azs.id), azs.addr, num ))
    for num in re.findall (r'#(\d+)', data):
        call = models.Call.query.filter(models.Call.id == num).first()
        if call:
            data = data.replace('#'+num, '<a href="{0}" title="{1}">заявка № {2}</a>'.format(url_for('requisition.call', id=call.id), call.call, num ))
    for num in re.findall (r'~(\d+)', data):
        page = Page.query.filter(Page.id == num).first()
        if page:
            data = data.replace('~'+num, ' <a href="{0}">{1}</a> '.format(url_for('page_editor.page', id=page.id), page.title))
    return data
