# coding: utf-8
import os
import importlib
import random
import logging
from flask_script import Command

logger = logging.getLogger()


def load_from_packages(app):
    pass


def load_from_folder(app):
    blueprints_path = app.config.get('BLUEPRINTS_PATH', 'modules')
    if not os.path.exists(blueprints_path):
        return
    path = os.path.join(
        app.config.get('BASEDIR', '..'),
        blueprints_path
    )
    base_module_name = blueprints_path #".".join([app.name, blueprints_path])
    dir_list = os.listdir(path)
    dir_list.sort()
    mods = {}
    object_name = app.config.get('BLUEPRINTS_OBJECT_NAME', 'module')
    for fname in dir_list:
        if not os.path.exists(os.path.join(path, fname, 'DISABLED')) and  \
                os.path.isdir(os.path.join(path, fname)) and \
                os.path.exists(os.path.join(path, fname, '__init__.py')):

            # register blueprint object
            module_name = ".".join([base_module_name, fname])

            try:
                mods[fname] = importlib.import_module(module_name)
                #~ print( "plugin load: {}".format(module_name) )
            except Exception as error:
                message = 'error import module [{0}] from {1}: [{2}]'.format(fname, path, error)
                print (message)
                #raise ImportError('error import module [{0}] from {1}'.format(fname, path))
                continue
            try:
                blueprint = getattr(mods[fname], object_name)
            except Exception as error:
                print ('no load', fname, object_name, error)

            if blueprint.name not in app.blueprints:
                app.register_blueprint(blueprint)
            else:
                blueprint.name += str(random.getrandbits(8))
                app.register_blueprint(blueprint)
                logger.warning(
                    "CONFLICT:{0} already registered, using {1}".format(
                        fname, blueprint.name
                    )
                )


    logger.info("{0} modules loaded".format(mods.keys()))

def load_admin_from_folder(app):
    blueprints_path = app.config.get('BLUEPRINTS_PATH', 'modules')
    if not os.path.exists(blueprints_path):
        return
    path = os.path.join(
        app.config.get('BASEDIR', '..'),
        blueprints_path
    )
    base_module_name = blueprints_path #".".join([app.name, blueprints_path])
    dir_list = os.listdir(path)
    dir_list.sort()
    mods = {}
    object_name = app.config.get('BLUEPRINTS_OBJECT_NAME', 'module')
    for fname in dir_list:
        if not os.path.exists(os.path.join(path, fname, 'DISABLED')) and  \
                os.path.isdir(os.path.join(path, fname)) and \
                os.path.exists(os.path.join(path, fname, '__init__.py')):

            # register admin
            module_name = ".".join([base_module_name, fname, 'admin'])
            try:
                importlib.import_module(module_name)
                #~ print( "admin load: {}".format(module_name) )
            except ImportError as error:
                logger.info(
                    "{0} module does not define admin: {1}".format(fname, str(error))
                )

    logger.info("{0} modules loaded".format(mods.keys()))


def load_blueprint_commands(manager):
    app = manager.app
    blueprints_path = app.config.get('BLUEPRINTS_PATH', 'modules')
    path = os.path.join(
        app.config.get('PROJECT_ROOT', '..'),
        blueprints_path
    )
    base_module_name = ".".join([app.name, blueprints_path])
    dir_list = os.listdir(path)
    mods = {}
    for fname in dir_list:
        if not os.path.exists(os.path.join(path, fname, 'DISABLED')) and  \
                os.path.isdir(os.path.join(path, fname)) and \
                os.path.exists(os.path.join(path, fname, '__init__.py')):

            # register management commands
            module_name = ".".join([base_module_name, fname])
            try:
                mod = importlib.import_module(
                    ".".join([module_name, 'commands'])
                )
                mods[fname] = mod
                for obj_name in dir(mod):
                    obj = getattr(mod, obj_name)
                    if obj_name != 'Command' and type(obj) == type and \
                            issubclass(obj, Command):
                        name = getattr(obj, 'command_name', obj_name.lower())
                        if name in manager._commands:
                            name += str(random.getrandbits(8))
                            logger.info("registering command {0}".format(name))
                        manager.add_command(name, obj())
            except ImportError:
                logger.info(
                    "{0} module does not define commands".format(fname)
                )
    logger.info("{0} management commands loaded".format(mods.keys()))
