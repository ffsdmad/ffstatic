import logging, smtplib, time
from functools import wraps
from threading import Thread
from flask import flash
from email.mime.text import MIMEText
from email.message import Message
from email.charset import Charset, QP
from .app import application



class SendMail(object):
    """отправка почты через SMTP
        необходимо чтобы в конфиге были опции:
        MAIL_SERVER
        MAIL_LOGIN
        MAIL_PASSWORD
        TARGET_MAIL
        необходимо чтобы у объекта были аттрибуты: obj_subject и obj_mail
        """

    TARGET_MAIL = application.config.get('TARGET_MAIL')
    config = {}

    def __precheck__(func):
        """преднастройки объекта отправки почты"""
        def _wrapper(self):
            self.config = self.get_mail_config()
            if self.obj_subject and self.obj_mail:
                func(self)
        return _wrapper

    def __run_async__(func):
        """выполнение отправки почты в потоке"""
        def async_func(self):
            def _f(self):
                with application.app_context():
                    wait = application.config.get('MAIL_WAIT_REPETITION', 5)
                    for i in range(application.config.get('MAIL_COUNT_REPETITION', 3)):
                        if func(self):
                            return
                        wait += (i+1) * wait
                        application.logger.info('{0} повтор отправки письма через {1} секунд '.format(i+1, wait ))
                        time.sleep(wait)

                    # меняет статус заявки на 7, не доделано
                    self.set_replay()

            t = Thread(target=_f, args=(self, ), name="mail-sender-{0}".format(self.id))
            t.start()
            return t
        return async_func

    @__precheck__
    @__run_async__
    def send(self):
        msg = Message()
        config = self.config
        charset = Charset('utf-8')
        charset.header_encoding = None
        charset.body_encoding = None
        msg.set_charset(charset)
        msg.set_payload(self.obj_mail)
        SUBJ = application.config.get('MAIL_SUBJ_PREFIX')
        msg['Subject'] = (SUBJ  if SUBJ else '') + self.obj_subject
        msg['From'] = config.get('MAIL_LOGIN')
        msg['To'] = config.get('TARGET_MAIL')
        try:
            #~ raise NotImplementedError
            if config.get('MAIL_PORT') == 25: # SMTP
                PROTOCOL = smtplib.SMTP
            else:
                PROTOCOL = smtplib.SMTP_SSL

            with PROTOCOL(config.get('MAIL_SERVER')) as serv:
                serv.login(config.get('MAIL_LOGIN'),
                        config.get('MAIL_PASSWORD'))

                rez = serv.sendmail(config.get('MAIL_LOGIN'),
                        config.get('TARGET_MAIL'),
                        msg.as_string().encode("utf8"))

                serv.quit()
                self.add_log(change="письмо отправлено")
                application.logger.info('письмо "{0}" отправлено'.format(msg['Subject']))
                return True
        except Exception as error:
            application.logger.error('ошибка [{0}] отправки письма: {1}'.format(error, self.id) )



    def get_mail_config(self, *args, **kwargs):
        return application.config


#~ def send(frm='order@dkmt.ru', to='ffsdmad@gmail.com', subj=u'test', body=u'проверка связи\т'*10):
    #~ from email import message_from_string
    #~ with smtplib.SMTP_SSL('mail.dkmt.ru') as s:
        #~ s.login(frm, 'Order_2')
        #~ s.sendmail(frm,to, message_from_string(body))

