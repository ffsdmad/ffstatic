# -*- coding: utf8 -*-
from flask_sqlalchemy import SQLAlchemy

import warnings
from sqlalchemy import exc as sa_exc

with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=sa_exc.SAWarning)

db = SQLAlchemy()


def _sql(fn):
    def wrapped(*args):
        return db.session.execute( fn(*args) )
    return wrapped
