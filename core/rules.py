from flask import abort, flash
from flask.ext.login import current_user
from flask.ext.principal import Principal, Permission, RoleNeed, UserNeed, PermissionDenied

from collections import namedtuple
from functools import partial

from .app import application

principals = Principal(application)

admin = Permission(RoleNeed('admin'))
chief = Permission(RoleNeed('chief'))
dispatcher = Permission(RoleNeed('dispatcher'))
supervision = Permission(RoleNeed('supervision'))
manager = Permission(RoleNeed('manager'))
executor = Permission(RoleNeed('executor'))
file_admin = Permission(RoleNeed('file_admin'))
editor = Permission(RoleNeed('editor'))
foma = Permission(UserNeed('foma'))

admin_chief = Permission(RoleNeed('admin'), RoleNeed('chief'))
admin_executor = Permission(RoleNeed('admin'), RoleNeed('executor'))


def user_required(f):
    """Checks whether user is logged in or raises error 401."""
    def decorator(*args, **kwargs):
        try:
            executor.require()
        except PermissionDenied:
            abort(403)
        except Exception as error:
            flash(error)
            abort(500)
        if not current_user.is_authenticated:
            abort(403)
        return f(*args, **kwargs)
    return decorator

def admin_chief_required2(f):
    """Checks whether user is logged in or raises error 401."""
    def decorator(*args, **kwargs):
        try:
            with admin_chief.require():
                return f(*args, **kwargs)
        except PermissionDenied:
            abort(403)
        except Exception as error:
            raise NotImplementedError
            flash(error)
            abort(500)
        if not current_user.is_authenticated:
            abort(403)
        return f(*args, **kwargs)
    return decorator

def admin_chief_required(f):
    """Checks whether user is logged in or raises error 401."""
    def decorator(*args, **kwargs):
        if admin_chief.can():
            return f(*args, **kwargs)
        abort(403)
    return decorator


def roles_required(*roles):
    def check_required(f):
        def decorator(*args, **kwargs):
            for role in roles:
                if role.can():
                    return f(*args, **kwargs)
            abort(403)
        return decorator
    return check_required
