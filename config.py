# -*- coding: utf8 -*-
import os
import subprocess
try:
    VERSION = int( subprocess.check_output(['git', 'rev-list', '--all', '--count']) )
except:
    VERSION = 999

PROJECT_ROOT = BASEDIR = os.path.abspath(os.path.dirname(__file__))
#~ MEDIA = os.path.join(BASEDIR, 'media')
#~ TEMPLATE_FOLDER = os.path.join(BASEDIR, 'templates')
#~ STATICFILES_DIRS = (os.path.join(BASEDIR, "static"))
#~
#~
MEDIA_FOLDER = 'media'
#~ MEDIA_URL = '/media/'


MEDIA_IMAGE_ALLOWED_EXTENSIONS = ('jpg', 'jpeg', 'png', 'tiff', 'gif', 'bmp')


MAX_CONTENT_LENGTH = 1024 * 1024 * 10

CSRF_ENABLED = not True
SECRET_KEY = os.urandom(20)

BLUEPRINTS_PATH = 'modules'
SQLALCHEMY_TRACK_MODIFICATIONS = True

DEBUG_TB_INTERCEPT_REDIRECTS = False

# пауза при перенаправлени на другую страницу
HEADER_REFRESH = 60

# записей на страницу
PER_PAGE = 30

BOWER_COMPONENTS_ROOT = 'bower_components'

try:
    from debug_config import *
    print (' ** DEBUG')
except:
    print (' ** DEPLOY')

IMAGE_404='media/nopic.jpg'
