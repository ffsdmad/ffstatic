# -*- coding: utf8 -*-
import logging
from logging.handlers import RotatingFileHandler
from datetime import datetime
from flask import Flask, json, Markup, g, abort, url_for, redirect
from flask import render_template, make_response, request, current_app
from flask_thumbnails import Thumbnail

from core.app import application





def create_app():

    if application.config['CSRF_ENABLED']:
        from flask_wtf.csrf import CsrfProtect
        csrf = CsrfProtect()
        csrf.init_app(application)


    # загружаем модули
    from core.load_blueprints import load_from_folder as load_blueprints, load_admin_from_folder as load_admin
    load_blueprints(application)

    # включаем Rest API
    from core.load_api import load_from_folder as load_api
    load_api(application)

    if application.config.get('DEBUG',False):
        from flask_debugtoolbar import DebugToolbarExtension
        toolbar = DebugToolbarExtension(application)
        #~ from flask import send_from_directory
        #~ @application.route('/media/<path:path>')
        #~ def media(path):
            #~ return send_from_directory(application.config['MEDIA_FOLDER'], path)

    thumb = Thumbnail(application)

    from . import views



    return application

@application.errorhandler(403)
def forbidden(error):
    refresh = application.config.get('HEADER_REFRESH',5)
    response = make_response(render_template('403.html',
            error=error, refresh = refresh )
        )
    response.headers.add('Refresh', "{0};{1}".format(refresh, '/') )
    response.headers.add('Location', '/')
    return response, 403

#~ @application.errorhandler(404)
#~ def forbidden(error):
    #~ refresh = application.config.get('HEADER_REFRESH',5)
    #~ response = make_response(render_template('404.html',
            #~ error=error, refresh = refresh, )
        #~ )
    #~ response.headers.add('Refresh', "{0};{1}".format(refresh, '/') )
    #~ response.headers.add('Location', '/')
    return response, 404

@application.errorhandler(500)
def server_error(error):
    application.logger.error(error)
    refresh = application.config.get('HEADER_REFRESH',5)
    response = make_response(render_template('500.html',
            error=error, refresh = refresh, )
        )
    response.headers.add('Refresh', "{0};{1}".format(refresh, '/') )
    response.headers.add('Location', '/')
    return response, 500
    #return redirect(url_for('admin.index'))

