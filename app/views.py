# -*- coding: utf8 -*-
import os, re, datetime, json, time, locale
import urllib, importlib
from functools import wraps
try:
    import urllib.request as urllib2
    from urllib.parse import quote
except ImportError:
    import urllib2
    from urllib import quote

from flask import g, Flask, request, json, Markup, redirect, jsonify
from flask_thumbnails import Thumbnail

try:
    from PIL import Image, ImageOps
    from PIL import ImageFile
    ImageFile.LOAD_TRUNCATED_IMAGES = True
except ImportError:
    raise RuntimeError('Image module of PIL needs to be installed')

from core.app import application

def __bg_square(img, color=0xff):
    """переопредляю сбойную функцию"""
    size = (max(img.size),) * 2
    layer = Image.new('L', size, color)
    layer.paste(img, tuple(map(lambda x: int((x[0] - x[1]) / 2), zip(size, img.size))))
    return layer


def called_counter(fun):
    """считаем количество вызовов"""
    @wraps(fun)
    def wrapper(*args, **kwargs):
        wrapper.called += 1
        return fun(*args, **kwargs)
    wrapper.called = 0
    return wrapper

def download_original_image(static_host, host, url, image_404):
    """скачивает ссылку, возвращает путь к файлу"""

    dirname = os.path.dirname( os.path.join( application.config.get('MEDIA_FOLDER'), static_host, url ) )
    if not os.path.exists(dirname):
        os.makedirs( dirname )

    filename = os.path.join(dirname, os.path.basename(url))


    if not os.path.exists( filename ):
        try:
            host_url = '/'.join( [host, url] )
            host_url = host_url.encode('utf-8')
            host_url = quote( host_url )
            response = urllib2.urlopen( 'http://' + host_url )
            open( filename, 'wb' ).write(response.read())
        except Exception as error:
            print (error,  [host, url] )
            try:
                response = urllib2.urlopen( image_404 )
                filename = os.path.join(application.config.get('MEDIA_FOLDER'), '_'.join([host,  '404.jpg']))
                open( filename, 'wb' ).write(response.read())
            except Exception as error:
                print (error)
                filename = application.config.get('IMAGE_404')

    print (filename)

    return filename

def get_args_image(url):
    """возвращает параметры преобразования изображения"""
    if url:
        _ = re.search('/?(\d+x\d+[^/]+)/', url)
        if _:
            return _.groups()[0]

def parse_args(args, size, crop, quality, background):
    """разбор параметров"""
    if args and re.match('([^_]+)', args):
        __ = re.findall('([^_]+)', args)
        if len(__)>0:
            size = __[0]
        if len(__)>1:
            crop = __[1]
        if len(__)>2:
            try:
                quality = int(__[2])
            except:
                pass
        if len(__)>3:
            try:
                background = int(__[3], 16)
            except:
                pass
    return dict(size=size, crop=crop, background=background, quality=quality)

def get_original_url(url, args):
    """возвращает ссылку на оригинальное изображение"""
    if args:
        url = url.split('/')
        url.remove(args)
        return '/'.join(url)
    return url
    _ = url.split('/')
    return '/'.join( _[:-2] + _[-1:])

def resize_image(path, size, crop, background, quality, **kwargs):
    """преобразует изображение по заданным параметрам"""
    thumb = Thumbnail(application)
    thumb._bg_square = __bg_square ## TODO, Hook
    return thumb.thumbnail(path, size=size, crop=crop, quality=quality,
#            bg=background
    )



@application.route('/')
def index():
    return '<body><a href="http://breys.ru/">go to breys.ru</body>', 200
    
@application.route('/<path:path>')
@called_counter
def search_file(path=None):
    static_host = request.environ.get('HTTP_HOST').split(':')[0]
    host = '.'.join(static_host.split('.')[1:])
    module_name = host.replace('.', '_')

    try:
        # загружаем индивидуальные параметры для конкретного сайта
        print('module_name', module_name)
        module = importlib.import_module(module_name)
    except ImportError as error:
        print(error)
        module = importlib.import_module('default_site')

    args = get_args_image(path)

    if hasattr( module, 'replace_host'):
        host = host.replace(module.replace_host[0], module.replace_host[1])
        print('replace_host', host )

    param = parse_args(args,
            size=module.size, crop=module.crop,
            quality=module.quality, background=module.background)

    original_url = get_original_url(path, args)
    print('original_url', original_url)

    file_path = download_original_image(static_host, host, original_url, module.image_404)
    print('file_path', file_path)

    thumb_path = resize_image(file_path[6:], **param)
    print( 'thumb_path', thumb_path)

    newfile = os.path.join( application.config.get('MEDIA_FOLDER'), static_host, path )
    dirname = os.path.dirname( newfile )
    if not os.path.exists(dirname):
        os.makedirs( dirname )
    print( 'os.rename',  application.config.get('MEDIA_FOLDER'), thumb_path, newfile )
    if thumb_path and newfile:
        os.rename(application.config.get('MEDIA_FOLDER') + thumb_path, newfile)
        print('redirect( {} )'.format(path))
        return redirect( path )
    return redirect( module.image_404 )
    return '<body>{}</body>'.format(locals()), 200

@application.route('/info/')
@called_counter
def api_info():
    """сводоная информация по работе микросервиса
    """
    version = application.config.get('VERSION')
    search_file_called = search_file.called
    return jsonify( **locals() )
