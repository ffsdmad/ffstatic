import os, sys
from PIL import Image

size = 400, 360

def open_image(name):
    print(name)
    img = Image.open(open(name, 'rb'))
    img.show()
    img.thumbnail(size, Image.ANTIALIAS)
    img.show()
    img.save('media/static.frenchcake.ru/' + os.path.basename(name))


for s in sys.argv[1:]:
    open_image(s)
