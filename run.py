#!/usr/bin/env python
import sys
from app import create_app, application as app



if __name__ == '__main__':
    app = create_app()
    if len(sys.argv)==1:
        app.run(host='0.0.0.0',debug=True, use_reloader=True )
    else:
        from flask_script import Manager

        manager = Manager(app)

        manager.run()
